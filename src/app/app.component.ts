import { Component, ElementRef, Renderer2 } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage:any = HomePage;

  constructor(private platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, element: ElementRef, renderer: Renderer2) {

    platform.ready().then(() => {

      if(platform.is('core')){
        renderer.addClass(element.nativeElement, 'desktop');
      }

    });

  }

}

